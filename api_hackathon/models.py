from os import environ
from pathlib import Path

import peewee

_ROOT_DIR = (Path(__file__).parent / "..").resolve()
STATE_DIR = Path(environ.get("STATE_DIRECTORY", _ROOT_DIR))
db = peewee.SqliteDatabase(str(STATE_DIR / "submissions.db"))


class APIKey(peewee.Model):
    """
    Store valid API keys
    """

    api_key = peewee.CharField()
    team_name = peewee.CharField(null=True)

    class Meta:
        database = db


class Submission(peewee.Model):
    """
    Store a single submission result in database.
    """

    api_key = peewee.CharField()
    team_name = peewee.CharField()
    score = peewee.FloatField()
    final_score = peewee.FloatField()
    timestamp = peewee.TimestampField()
    raw_submission = peewee.TextField()

    class Meta:
        database = db
