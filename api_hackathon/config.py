from pydantic import BaseSettings, Field


class Config(BaseSettings):

    redis_url: str = Field("redis://localhost", description="url of the server used for the rate limiting")
    max_repetition: int = Field(10, description="Maximum number of calls to the API for the given time")
    time_interval: int = Field(60 * 60 * 24, description="Time interval for the max repetition (in seconds) ")

    class Config:
        env_file = ".env"


env_config = Config()
