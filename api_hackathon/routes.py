import collections
import io
import logging
import os
import random
import time
from typing import Optional

import sklearn.metrics
from fastapi import FastAPI, File, Form, UploadFile, HTTPException, status
from fastapi.middleware.cors import CORSMiddleware
from playhouse.shortcuts import model_to_dict

from .config import env_config
from .models import APIKey, Submission
from .rate_limiter import check_rate_availability
from .utils import parse_truth_csv, read_sorted_csv_dict

FIXED_SEED = 424242

APP = FastAPI()

origins = ["*"]

APP.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@APP.post("/submit")
def post_submission(api_key: str = Form(), team_name: Optional[str] = Form(None), file: UploadFile = File(...)):
    """
    Post a new submission (multipart/form body).

    :param api_key: API key for submission.
    :param team_name: Name of the submitting team.
    :param file: Uploaded file.
    """
    error_message = "An error occured while processing your submission"
    try:
        # Check API key exists
        full_api_key = APIKey.get(APIKey.api_key == api_key)
        if not full_api_key:
            raise AssertionError("Invalid API key")
        team_name = team_name or full_api_key.team_name

        # Team name should not be empty
        if not team_name:
            raise AssertionError("Missing team name")

        check_rate_availability(
            rate_key=full_api_key.api_key,
            redis_url=env_config.redis_url,
            max_repetition=env_config.max_repetition,
            time_interval=env_config.time_interval,
        )

        # Read file upload and CSV-parse it
        raw_full_csv_file = file.file.read().decode("utf-8")
        csv_file = io.StringIO(raw_full_csv_file)
        parsed_csv_file = read_sorted_csv_dict(csv_file, "observation_uuid")

        # Get the truth CSV file
        truth_csv_file = parse_truth_csv()

        # Participants are expected to provide a CSV file with the correct header
        if list(parsed_csv_file[0].keys()) != ["observation_uuid", "algorithm"]:
            raise AssertionError("Header of provided CSV should match observation_uuid,algorithm")

        # Participants are expected to provide a complete CSV file
        if len(parsed_csv_file) != len(truth_csv_file):
            raise AssertionError("Length of provided CSV do not match length of truth CSV")

        # Score is computed on a sample of the truth_csv_file
        sample_length = int(0.2 * len(truth_csv_file))
        # Note: Both CSV files are expected to have same length and be ordered
        # in the same way. Therefore, we can just apply random.sample twice,
        # provided we have a common fixed seed (must be repeated at each random
        # operation).
        random.seed(FIXED_SEED)
        sample_truth_csv_file = random.sample(truth_csv_file, k=sample_length)
        random.seed(FIXED_SEED)
        sample_parsed_csv_file = random.sample(parsed_csv_file, k=sample_length)

        # Compute score
        leaderboard_score = sklearn.metrics.adjusted_rand_score(
            [item["algorithm"] for item in sample_truth_csv_file],
            [item["algorithm"] for item in sample_parsed_csv_file],
        )
        final_score = sklearn.metrics.adjusted_rand_score(
            [item["algorithm"] for item in truth_csv_file],
            [item["algorithm"] for item in parsed_csv_file],
        )

        # Store submission in db
        Submission.create(
            api_key=api_key,
            team_name=team_name,
            score=leaderboard_score,
            final_score=final_score,
            timestamp=time.time(),
            raw_submission=raw_full_csv_file,
        )

        return {
            "status": "ok",
            "score": leaderboard_score,
        }
    except Exception as exc:
        logging.error("Exception found upon submission: %s.", str(exc))
        return {"status": "nok", "error": error_message}


@APP.get("/leaderboard")
def get_leaderboard():
    """
    Get the current leaderboard.
    """
    submissions_by_team = collections.defaultdict(list)
    # Group submissions by team
    for submission in Submission.select():
        submissions_by_team[submission.team_name].append(
            {
                k: v
                for k, v in model_to_dict(submission).items()
                if k not in ["raw_submission", "final_score", "api_key"]
            }
        )
    # Compute leaderboard
    leaderboard = [
        max(submissions, key=lambda item: item["score"])
        | {"nb_submissions": len(submissions)}
        | {"last_submission": max([submission["timestamp"] for submission in submissions])}
        for submissions in submissions_by_team.values()
    ]
    return leaderboard


@APP.get("/final_leaderboard")
def get_final_leaderboard(api_key: Optional[str] = None):
    """
    Get the final leaderboard (including final score).
    """
    ENV_API_KEY = os.environ.get("FINAL_LEADERBOARD_API_KEY")
    if ENV_API_KEY and api_key != ENV_API_KEY:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid authentication credentials",
        )
    return [
        {k: v for k, v in model_to_dict(submission).items() if k != "raw_submission"}
        for submission in Submission.select()
    ]
