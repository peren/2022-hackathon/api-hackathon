__version__ = "0.1"
import logging

from .models import db, APIKey, Submission
from .routes import APP

logging.basicConfig(level=logging.WARNING)

# Initialize database
try:
    db.connect()
    db.create_tables([APIKey, Submission])
finally:
    db.close()

app = application = APP
