import csv
import functools


def read_sorted_csv_dict(csv_iterable, sorting_column):
    """
    Read a CSV dict and sort rows according to the specified column.

    :param csv_iterable: A CSV file handler or StringIO.
    :param sorting_column: Column name to sort on.
    :returns: Sorted list of dicts.
    """
    parsed_csv_file = csv.DictReader(csv_iterable, delimiter=",")
    return sorted(parsed_csv_file, key=lambda item: item[sorting_column])


@functools.lru_cache(maxsize=1)
def parse_truth_csv(path="./truth.csv"):
    """
    Read and parse the CSV truth.

    :param path: Path to the truth CSV, defaults to "./truth.csv".
    :returns: Sorted list of dicts.
    """
    with open(path, "r") as fh:
        return read_sorted_csv_dict(fh, "observation_uuid")
