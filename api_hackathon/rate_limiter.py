#
# Pôle d'Expertise de la Régulation Numérique (PEReN) CONFIDENTIAL
# _____________________
#
# Copyright © Pôle d'Expertise de la Régulation Numérique (PEReN) 2020-2022
# All Rights Reserved.
#
# AVERTISSEMENT : Toutes les informations et outils présents dans
# ce fichier sont et restent la propriété du Pôle d'Expertise de la Régulation Numérique (PEReN). La diffusion
# ou la reproduction de ces éléments est strictement interdite sans
# l'accord écrit du Pôle d'Expertise de la Régulation Numérique (PEReN).
#

# strongly inspired from https://github.com/long2ice/fastapi-limiter

import redis
from fastapi import HTTPException
from starlette.status import HTTP_429_TOO_MANY_REQUESTS


def get_time_to_next_call(redis_interface, rate_key, max_repetition, time_interval):
    """
    Get the time to next call for the given rate_key. 0 if it is available, a time in milliseconds otherwise.
    Nb: set a limit from the first call, so it is possible to do 2*max_repetition-1 call in a very short time frame
    The way it works is that it instantiates a counter with key [rate_key], with a time to live of [time interval]
    the counter starts at 1 and increments with each call to the [rate_key], up to the point where the counter reach
    [max_repetition], at which point we return the leftover time to live.
    If the key doesn't have a ttl, this is a bug from a previous call, so we remove the key, and lock it for a time
    of [time_interval] (by setting the counter directly to [max_repetition] with ttl [time_interval]).

    :param redis_interface: interface to call redis
    :param rate_key: key for the limiter. typically a module name
    :param max_repetition: the max number of repetitions allowed in the time interval
    :param time_interval: the time interval for rate limiting in seconds
    :returns: 0 if call is available, the time to wait in milliseconds otherwise
    """
    redis_value = redis_interface.get(rate_key)
    if time_interval is None:  # Shouldn't happen if config have been set up correctly
        raise Exception(
            f"No time interval for {rate_key=}, this is a server configuration error, "
            f"contact an administrator to fix this"
        )
    if redis_value is None:  # key does not exist or ttl is expired
        redis_interface.set(rate_key, 1, time_interval)
        return 0
    if int(redis_value) >= max_repetition:
        time_to_live = redis_interface.pttl(rate_key)
        if time_to_live == -1:  # key do not have a pttl,
            redis_interface.delete(rate_key)  # keys without a pttl are not supposed to exist, we can safely delete it
            # we put a new key that will block calls to rate_key for time_interval
            redis_interface.set(rate_key, max_repetition, ex=time_interval)
        return redis_interface.pttl(rate_key)
    redis_interface.incr(rate_key)
    return 0


def check_rate_availability(rate_key, redis_url, max_repetition, time_interval):
    """
    Answer True if the key is available, raise a HTTPException HTTP_429_TOO_MANY_REQUESTS otherwise

    :param rate_key: key for the limiter. typically a concatenation of the module name and the user
    :param redis_url: url for the redis server
    :param max_repetition: the max number of repetitions allowed in the time interval
    :param time_interval: the time interval for rate limiting in seconds
    :returns: True if call is available, an exception with the time to wait in seconds otherwise
    """
    redis_interface = redis.Redis.from_url(redis_url)
    time_to_next_call = get_time_to_next_call(redis_interface, rate_key, max_repetition, time_interval)
    if time_to_next_call == 0:
        return True

    raise HTTPException(
        HTTP_429_TOO_MANY_REQUESTS, "Too Many Requests", headers={"Retry-After": str(time_to_next_call // 1000 + 1)}
    )
