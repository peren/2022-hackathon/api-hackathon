import argparse
from os import urandom
from typing import Dict, Optional

import uvicorn

from .models import db, APIKey
from .routes import APP


def _generate_temp_password(length):
    if not isinstance(length, int) or length < 8:
        raise ValueError("temp password must have positive length")
    chars = "ABCDEFGHJKLMNPQRSTUVWXYZ23456789"
    return "".join(chars[c % len(chars)] for c in urandom(length))


class UnknownCommandError(Exception):
    pass


def make_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(prog="api-hackathon", description="API de soumission du hackathon")

    subparsers = parser.add_subparsers(dest="cmd")

    serve_parser = subparsers.add_parser("serve", help="Serve the API in development mode.")
    serve_parser.add_argument("--host", default="127.0.0.1")
    serve_parser.add_argument("--port", type=int, default=3000)

    generate_api_key_parser = subparsers.add_parser(
        "generate-api-key", help="Generate and store a new API key to the database."
    )
    generate_api_key_parser.add_argument("--team-name", default=None)

    return parser


def run_with_args(args: argparse.Namespace) -> Optional[Dict]:
    if args.cmd == "serve":
        uvicorn.run(APP, host=args.host, port=args.port)
    elif args.cmd == "generate-api-key":
        api_key = _generate_temp_password(12)
        APIKey.create(api_key=api_key, team_name=args.team_name)
        print(api_key)
    else:
        raise UnknownCommandError(f"Unknown command: {args.cmd}")


if __name__ == "__main__":
    try:
        db.connect()
        # Run commands
        parser = make_parser()
        args = parser.parse_args()
        try:
            run_with_args(args)
        except UnknownCommandError:
            parser.print_help()
    finally:
        db.close()
