api-hackathon
=============

API de soumission du hackathon


## Description

API pour la soumission des résultats et le calcul du score / _leaderboard_
pour le hackathon.


## Installation

```bash
# Récupération du code avec Git
git clone ${GITLAB_URL}
cd api_hackathon

# Création d'un virtualenv et installation des dépendances requises
python3 -m virtualenv .venv
./.venv/bin/pip install poetry

# Installation des dépendances via poetry.
./.venv/bin/poetry install

# Mettre en place le fichier CSV de vérité
cp ./truth.example.csv ./truth.csv && $EDITOR ./truth.csv
```


## Utilisation

Pour obtenir l'aide :

```bash
./.venv/bin/python3 -m api_hackathon
```

Pour lancer l'API en mode développement :

```bash
./.venv/bin/python3 -m api_hackathon serve
```

Pour protéger l'accès au _leaderboard_ final par une clé d'API, passer la
variable d'environnement `FINAL_LEADERBOARD_API_KEY`.

Pour activer le rate_limiting, il faut passer les variables d'environnement en faisant cp `.env.exemple` `.env` et en modifiant le contenu de façon appropriée. 


### Exemple de bout en bout d'utilisation de l'API

#### Soumission

```bash
$ curl -X POST -F "api_key=API_KEY" -F "team_name=Toto" -F "file=@truth.csv" http://127.0.0.1:3000/submit | jq '.'
{
  "status": "ok",
  "score": 1
}
```

#### Obtention du _leaderboard_

```bash
$ curl http://127.0.0.1:3000/leaderboard | jq '.'
[
  {
    "id": 1,
    "team_name": "Toto",
    "score": 1,
    "timestamp": "2022-10-12T13:39:19"
  }
]
```

#### Obtention du _leaderboard_ final

```bash
$ curl http://127.0.0.1:3000/final_leaderboard?api_key=${FINAL_LEADERBOARD_API_KEY} | jq '.'
[
  {
    "id": 1,
    "team_name": "Toto",
    "score": 1,
    "final_score": 1,
    "timestamp": "2022-10-12T13:39:19"
  }
]
```



## Contribution

Avant de contribuer au dépôt, il est nécessaire d'initialiser les _hooks_ de _pre-commit_ :

```
pre-commit install
```


## Licence

Ce projet est sous licence MIT. Une copie intégrale du texte
de la licence se trouve dans le fichier [`LICENSE.md`](LICENSE.md).
